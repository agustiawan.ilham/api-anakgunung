﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataEntities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class DB_anak_gunungEntities : DbContext
    {
        public DB_anak_gunungEntities()
            : base("name=DB_anak_gunungEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ms_city> ms_city { get; set; }
        public virtual DbSet<ms_close_date> ms_close_date { get; set; }
        public virtual DbSet<ms_day> ms_day { get; set; }
        public virtual DbSet<ms_event> ms_event { get; set; }
        public virtual DbSet<ms_grant_comunity> ms_grant_comunity { get; set; }
        public virtual DbSet<ms_header> ms_header { get; set; }
        public virtual DbSet<ms_imagemount> ms_imagemount { get; set; }
        public virtual DbSet<ms_login_user> ms_login_user { get; set; }
        public virtual DbSet<ms_mountain> ms_mountain { get; set; }
        public virtual DbSet<ms_post> ms_post { get; set; }
        public virtual DbSet<ms_provinsi> ms_provinsi { get; set; }
        public virtual DbSet<ms_schedule> ms_schedule { get; set; }
        public virtual DbSet<ms_subheader> ms_subheader { get; set; }
        public virtual DbSet<ms_user> ms_user { get; set; }
    
        public virtual ObjectResult<SP_AllListEvent_Result> SP_AllListEvent()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_AllListEvent_Result>("SP_AllListEvent");
        }
    
        public virtual ObjectResult<SP_EventByPKEvent_Result> SP_EventByPKEvent(Nullable<long> pK_event)
        {
            var pK_eventParameter = pK_event.HasValue ?
                new ObjectParameter("PK_event", pK_event) :
                new ObjectParameter("PK_event", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_EventByPKEvent_Result>("SP_EventByPKEvent", pK_eventParameter);
        }
    
        public virtual ObjectResult<SP_EventByUserID_Result> SP_EventByUserID(Nullable<long> user_id)
        {
            var user_idParameter = user_id.HasValue ?
                new ObjectParameter("User_id", user_id) :
                new ObjectParameter("User_id", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_EventByUserID_Result>("SP_EventByUserID", user_idParameter);
        }
    
        public virtual ObjectResult<SP_Info_User_Result> SP_Info_User()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_Info_User_Result>("SP_Info_User");
        }
    
        public virtual ObjectResult<SP_Post_Result> SP_Post()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_Post_Result>("SP_Post");
        }
    
        public virtual ObjectResult<SP_SearchEvent_Result> SP_SearchEvent(string keyword)
        {
            var keywordParameter = keyword != null ?
                new ObjectParameter("keyword", keyword) :
                new ObjectParameter("keyword", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_SearchEvent_Result>("SP_SearchEvent", keywordParameter);
        }
    
        public virtual ObjectResult<SP_SelectMyFeed_Result> SP_SelectMyFeed(Nullable<long> userID)
        {
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("UserID", userID) :
                new ObjectParameter("UserID", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_SelectMyFeed_Result>("SP_SelectMyFeed", userIDParameter);
        }
    
        public virtual ObjectResult<SP_AllMountain_Result> SP_AllMountain()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_AllMountain_Result>("SP_AllMountain");
        }
    
        public virtual ObjectResult<SP_HeaderByMountain_Result> SP_HeaderByMountain(Nullable<long> pK_mountain)
        {
            var pK_mountainParameter = pK_mountain.HasValue ?
                new ObjectParameter("PK_mountain", pK_mountain) :
                new ObjectParameter("PK_mountain", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_HeaderByMountain_Result>("SP_HeaderByMountain", pK_mountainParameter);
        }
    
        public virtual ObjectResult<SP_MountainByPKMountain_Result> SP_MountainByPKMountain(Nullable<long> pK_mountain)
        {
            var pK_mountainParameter = pK_mountain.HasValue ?
                new ObjectParameter("PK_mountain", pK_mountain) :
                new ObjectParameter("PK_mountain", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_MountainByPKMountain_Result>("SP_MountainByPKMountain", pK_mountainParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> SP_PeopleCountbyMountain(Nullable<long> pK_mountain)
        {
            var pK_mountainParameter = pK_mountain.HasValue ?
                new ObjectParameter("PK_mountain", pK_mountain) :
                new ObjectParameter("PK_mountain", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SP_PeopleCountbyMountain", pK_mountainParameter);
        }
    
        public virtual ObjectResult<SP_PostByMountain_Result> SP_PostByMountain(Nullable<long> pK_mountain)
        {
            var pK_mountainParameter = pK_mountain.HasValue ?
                new ObjectParameter("PK_mountain", pK_mountain) :
                new ObjectParameter("PK_mountain", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_PostByMountain_Result>("SP_PostByMountain", pK_mountainParameter);
        }
    
        public virtual ObjectResult<Nullable<double>> SP_StarCountbyMountain(Nullable<long> pK_mountain)
        {
            var pK_mountainParameter = pK_mountain.HasValue ?
                new ObjectParameter("PK_mountain", pK_mountain) :
                new ObjectParameter("PK_mountain", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<double>>("SP_StarCountbyMountain", pK_mountainParameter);
        }
    }
}
