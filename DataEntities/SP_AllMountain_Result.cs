//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataEntities
{
    using System;
    
    public partial class SP_AllMountain_Result
    {
        public long PK_mountain_ID { get; set; }
        public Nullable<long> FK_city_ID { get; set; }
        public string mountName { get; set; }
        public string difficulty { get; set; }
        public string picMountain { get; set; }
        public Nullable<double> weather_min { get; set; }
        public Nullable<double> weather_max { get; set; }
        public Nullable<double> starCount { get; set; }
        public Nullable<long> peopleCount { get; set; }
        public long PK_city_ID { get; set; }
        public Nullable<long> FK_province_ID { get; set; }
        public string city_name { get; set; }
        public long PK_province_ID { get; set; }
        public string province_name { get; set; }
    }
}
