﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataEntities;

namespace AnakGunungAPI.Models
{
    public class ProfileModel
    {
        public class MyProfile
        {
            public SP_Info_User_Result MyInfo { get; set; }
            public List<SP_EventByUserID_Result> MyEvent { get; set; }
            public List<SP_SelectMyFeed_Result> MyFeed { get; set; }
        }

    }
}