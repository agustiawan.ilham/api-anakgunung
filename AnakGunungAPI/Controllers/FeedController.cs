﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataEntities;
using AnakGunungAPI.Models;
using AnakGunungAPI.Functions;

namespace AnakGunungAPI.Controllers
{
    public class FeedController : ApiController
    {
        [Route("api/feed/getRandomPost")]
        public HttpResponseMessage GetRandomPost()
        {
            try
            {
                var data = FeedEntities.getRandomPost();
                return Request.CreateResponse(HttpStatusCode.Created, data);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("api/feed/Post")]
        public HttpResponseMessage GetPostByPKPostID(int id)
        {
            try
            {
                var data = FeedEntities.getPostByPKPost(id);
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
