﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataEntities;
using AnakGunungAPI.Models;
using AnakGunungAPI.Functions;

namespace AnakGunungAPI.Controllers
{
    public class EventController : ApiController
    {
        [Route("api/event/event")]
        public HttpResponseMessage GetAllEvent()
        {
            try
            {
                var data = EventEntities.getAllEvent();
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("api/event/event")]
        public HttpResponseMessage GetEventByPKEvent(int id)
        {
            try
            {
                var data = EventEntities.getEventByUserId(id);
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("api/event/eventbyname")]
        public HttpResponseMessage GetEventByEventName(string keyword)
        {
            try
            {
                var data = EventEntities.getEventByEventName(keyword);
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("api/event/eventbyuser")]
        public HttpResponseMessage GetEventByUser(int id)
        {
            try
            {
                var data = EventEntities.getEventByUserId(id);
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


    }
}
