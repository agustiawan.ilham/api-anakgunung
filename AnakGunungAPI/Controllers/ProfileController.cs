﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataEntities;
using AnakGunungAPI.Models;
using AnakGunungAPI.Functions;

namespace AnakGunungAPI.Controllers
{
    public class ProfileController : ApiController
    {
        [Route("api/profile/getProfile")]
        public HttpResponseMessage GetAllProfile()
        {
            try
            {
                var data = FeedEntities.getRandomPost();
                return Request.CreateResponse(HttpStatusCode.Created, data);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("api/profile/getProfile")]
        public HttpResponseMessage getProfileByUserID(int id)
        {
            try
            {
                ProfileModel.MyProfile data = new ProfileModel.MyProfile();
                var myInfo = UserEntities.getUserInfoByUserID(id);
                var myEvent = EventEntities.getEventByUserId(id);
                var myFeed = FeedEntities.getPostByUserID(id);
                data.MyInfo = myInfo;
                data.MyEvent = myEvent;
                data.MyFeed = myFeed;
                return Request.CreateResponse(HttpStatusCode.Created, data);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
