﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataEntities;
using AnakGunungAPI.Models;

namespace AnakGunungAPI.Functions
{
    public class UserEntities
    {
        public static List<ms_user> getUser()
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.ms_user.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ms_user getUserByUserID(int id)
        {
            try
            {
                using(DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.ms_user.FirstOrDefault(e => e.PK_user_ID == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ms_login_user getLoginUserByPKLoginID (int id)
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.ms_login_user.FirstOrDefault(e => e.PK_login_user_ID == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SP_Info_User_Result getUserInfoByUserID (int id)
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.SP_Info_User().FirstOrDefault(e => e.PK_user_ID == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}