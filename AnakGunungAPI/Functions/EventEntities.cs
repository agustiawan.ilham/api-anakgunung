﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataEntities;
using AnakGunungAPI.Models;

namespace AnakGunungAPI.Functions
{
    public class EventEntities
    {
        public static List<SP_AllListEvent_Result> getAllEvent()
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.SP_AllListEvent().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SP_SearchEvent_Result> getEventByEventName (string keyword)
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.SP_SearchEvent(keyword).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SP_EventByPKEvent_Result getEventByPKEvent (int id)
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.SP_EventByPKEvent(id).FirstOrDefault(e => e.PK_event_ID == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SP_EventByUserID_Result> getEventByUserId (int id)
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.SP_EventByUserID(id).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}