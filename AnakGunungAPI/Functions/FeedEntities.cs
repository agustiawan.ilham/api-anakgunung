﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataEntities;
using AnakGunungAPI.Models;

namespace AnakGunungAPI.Functions
{
    public class FeedEntities
    {
        public static List<SP_Post_Result> getAllPost()
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.SP_Post().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SP_Post_Result getPostByPKPost(int id)
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.SP_Post().FirstOrDefault(e => e.PK_post_ID == id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<SP_Post_Result> getRandomPost()
        {
            try
            {
                using (DB_anak_gunungEntities entitites = new DB_anak_gunungEntities())
                {
                    var allPost = FeedEntities.getAllPost();
                    var randomPost = allPost.OrderBy(x => Guid.NewGuid()).ToList();
                    var randomPostPage = randomPost.GetRange(1, 3).ToList();
                    return randomPostPage;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static List<SP_SelectMyFeed_Result> getPostByUserID(int id)
        {
            try
            {
                using (DB_anak_gunungEntities entities = new DB_anak_gunungEntities())
                {
                    return entities.SP_SelectMyFeed(id).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}